import 'package:flutter/material.dart';

import './widgets/filters_widget.dart';
import './widgets/list_view/workshops_list_view.dart';
import './widgets/search_field_widget.dart';
import './widgets/bottom_nav_bar.dart';
import '../../models/workshop.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  int _currentIndex = 2;
  List<Workshop> _workshops = [
    Workshop(
      imageURL: 'https://picsum.photos/275',
      name: 'Al Amana Workshop',
      description: 'Mechanical - Car wash',
      distance: 15,
      rating: 3,
      favorite: true,
    ),
    Workshop(
      imageURL: 'https://picsum.photos/275',
      name: 'Moaz Workshop',
      description: 'Mechanical - Car Electornics',
      distance: 5,
      rating: 5,
      favorite: true,
    ),
    Workshop(
      imageURL: 'https://picsum.photos/275',
      name: 'Random Workshop',
      description: 'Car Tires',
      distance: 25,
      rating: 2,
      favorite: false,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFBE1D2D),
        title: Text('Search'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            //
          },
        ),
      ),
      body: Column(
        children: [
          SearchField(),
          FiltersWidget(),
          WorkshopsListView(workshops: _workshops),
        ],
      ),
      bottomNavigationBar: BottomNavBar(
        currentIndex: _currentIndex,
        onTap: (index) => setState(() => _currentIndex = index),
      ),
    );
  }
}
