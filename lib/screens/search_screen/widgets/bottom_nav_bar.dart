import 'package:flutter/material.dart';

class BottomNavBar extends StatelessWidget {
  final int currentIndex;
  final Function onTap;

  BottomNavBar({this.currentIndex, this.onTap});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      iconSize: 25,
      selectedIconTheme: IconThemeData(size: 30),
      currentIndex: currentIndex,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Color(0xFFBE1D2D)),
            title: Text('Home')),
        BottomNavigationBarItem(
            icon: Icon(Icons.favorite, color: Color(0xFFBE1D2D)),
            title: Text('Favorite')),
        BottomNavigationBarItem(
            icon: Icon(Icons.search, color: Color(0xFFBE1D2D)),
            title: Text('Search')),
        BottomNavigationBarItem(
            icon: Icon(Icons.chat, color: Color(0xFFBE1D2D)),
            title: Text('Chat')),
        BottomNavigationBarItem(
            icon: Icon(Icons.person, color: Color(0xFFBE1D2D)),
            title: Text('Profile')),
      ],
      onTap: onTap,
    );
  }
}
