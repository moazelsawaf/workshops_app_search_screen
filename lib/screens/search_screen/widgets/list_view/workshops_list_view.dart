import 'package:flutter/material.dart';

import '../../../../models/workshop.dart';
import './list_view_item.dart';


class WorkshopsListView extends StatelessWidget {

  final List<Workshop> workshops;

  WorkshopsListView({@required this.workshops});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: workshops.length,
        itemBuilder: (context,index)=>
        ListViewItem(
          imageURL: workshops[index].imageURL,
          name: workshops[index].name,
          description: workshops[index].description,
          distance: workshops[index].distance,
          rating: workshops[index].rating,
          favorite: workshops[index].favorite,
        ),
      ),
    );
  }
}
