import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      child: TextField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(0),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFFBE1D2D)),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFFBE1D2D)),
            borderRadius: BorderRadius.circular(10),
          ),
          hintText: 'Search by Service Name',
          hintStyle: TextStyle(color: Colors.grey.shade400),
          prefixIcon: Icon(
            Icons.search,
            color: Colors.grey.shade400,
          ),
        ),
      ),
    );
  }
}
