import 'package:flutter/material.dart';

class CustomChoiceChip extends StatelessWidget {
  final String label;

  CustomChoiceChip({this.label});

  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        backgroundColor: Color(0xFFDAEEDE),
        selectedColor: Color(0xFF67BE7A),
        label: Text(label),
        labelStyle: TextStyle(color: Colors.black),
        selected: false,
        onSelected: (selected) {
          //
        });
  }
}
