import 'package:flutter/material.dart';

import './custom_choice_chip.dart';

class FiltersWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        IntrinsicHeight(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(
                  Icons.filter_list,
                  color: Color(0xFFBE1D2D),
                ),
                CustomChoiceChip(label: 'Rating'),
                CustomChoiceChip(label: 'Nearest'),
                CustomChoiceChip(label: 'Service Type'),
                Icon(
                  Icons.location_on,
                  color: Color(0xFFBE1D2D),
                ),
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          width: double.infinity,
          child: Wrap(
            alignment: WrapAlignment.start,
            spacing: 16,
            children: <Widget>[
              CustomChoiceChip(label: 'ونش'),
              CustomChoiceChip(label: 'مغسلة'),
              CustomChoiceChip(label: 'ورشة'),
              CustomChoiceChip(label: 'توكيلات'),
            ],
          ),
        )
      ],
    );
  }
}
