class Workshop {
  String imageURL;
  String name;
  String description;
  int distance;
  int rating;
  bool favorite;

  Workshop({
    this.imageURL,
    this.name,
    this.description,
    this.distance,
    this.rating,
    this.favorite,
  });
}
